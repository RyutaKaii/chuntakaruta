import requests
from xml.etree.ElementTree import Element, SubElement, tostring
import os
import time

# ====================================================================================================
# 定数
# ====================================================================================================
# AZURE token取得URL
URL_AZURE_GET_TOKEN = 'https://japaneast.api.cognitive.microsoft.com/sts/v1.0/issueToken'
# AZURE サブスクリプションキー
KEY_AZURE_SUB = '2c889964b7be4c5b8f3104f6ef29745a'
# AZURE 音声合成APIエンドポイント(東日本)
ENDPOINT_TEXT_TO_SPEACH = 'https://japaneast.tts.speech.microsoft.com/cognitiveservices/v1'
# AZURE 音声形式
FORMAT_TEXT_TO_SPEACH = 'audio-16khz-32kbitrate-mono-mp3'
# AZURE テキスト言語
SPEAK_LANG = 'ja-jp'
# AZURE 音声言語
VOICE_LANG = 'ja-jp'
# AZURE 音声性別
VOICE_GENDER_STAT = 'Female'
# AZURE 音声名
VOICE_NAME = 'ja-JP-NanamiNeural'

# ====================================================================================================
# 音声取得API
# 
# ARGS
#   text テキスト
#   dlPath ダウンロードパス
# RETURN
#   なし
# THROW
#   例外
# 参考
#   AZURE 音声合成API https://docs.microsoft.com/ja-jp/azure/cognitive-services/speech-service/overview#try-the-speech-service-for-free
# ====================================================================================================
def dlSound(text, dlPath):
    try:
        print('==================== START getSoundFromApi ====================')
        print('text' + str(text))
        print('dlPath' + str(dlPath))

        token = getToken()

        if not token == '':
            res = getSound(token, text, dlPath)

            if res:
                return

        raise Exception('ERROR DOWNLOAD SOUND')
    except Exception as e:
        print('==================== ERROR dlSound ====================')
        raise e

# ====================================================================================================
# トークン取得
# ====================================================================================================
def getToken():
    token = ''

    headers = {
        'Ocp-Apim-Subscription-Key': KEY_AZURE_SUB
    }

    res = requests.post(URL_AZURE_GET_TOKEN, headers=headers)
    if res.status_code == 200:
        print('SUCCESS GET TOKEN FROM API.')
        token = str(res.text)
        print('token: ' + token)
    else:
        print('ERROR GET TOKEN FROM API. status_code = ' + str(res.status_code))

    return token

# ====================================================================================================
# 音声取得
# ====================================================================================================
def getSound(token, text, dlPath):
    headers = {
        'Authorization': 'Bearer ' + str(token),
        'X-Microsoft-OutputFormat': FORMAT_TEXT_TO_SPEACH,
        'Content-Type': 'application/ssml+xml'
    }

    body = createXmlStr(SPEAK_LANG, VOICE_LANG, VOICE_GENDER_STAT, VOICE_NAME, text)

    res = requests.post(ENDPOINT_TEXT_TO_SPEACH, body.encode('utf-8'), headers=headers)
    if res.status_code == 200:
        with open(dlPath, 'wb') as f:
            print('SUCCESS GET SOUND FROM API.')
            f.write(res.content)
    else:
        print('ERROR GET SOUND FROM API. status_code = ' + str(res.status_code))
        return False
    
    return True

# ====================================================================================================
# xml文字列生成
# 
# bodyサンプル
# <speak version='1.0' xml:lang='ja-jp'><voice xml:lang='ja-jp' xml:gender='Female' name='en-US-AriaNeural'>テスト文字列</voice></speak>
# ====================================================================================================
def createXmlStr(speakLang, voiceLang, voiceGender, voiceName, text):
    speak = Element('speak', {'version':'1.0' ,'xml:lang':speakLang})
    voice = SubElement(speak, 'voice', {'xml:lang':voiceLang, 'xml:gender':voiceGender ,'name':voiceName})
    voice.text = text

    xmlStr = tostring(speak).decode()
    
    print('xml = ' + xmlStr)

    return str(xmlStr)

# ====================================================================================================
# 直接実行時
# ====================================================================================================
if __name__ == "__main__":
    dlSound('そでふりあうもたしょうのえん', os.path.dirname(__file__) + os.sep + 'そ.mp3')
    time.sleep(3)
    dlSound('わをもってとうとしとなす', os.path.dirname(__file__) + os.sep + 'わ.mp3')
    time.sleep(3)
    dlSound('るすみまいはまどおにせよ', os.path.dirname(__file__) + os.sep + 'る.mp3')
    time.sleep(3)
    dlSound('はやおきはさんもんのとく', os.path.dirname(__file__) + os.sep + 'は.mp3')
    time.sleep(3)
    dlSound('ちくばのとも', os.path.dirname(__file__) + os.sep + 'ち.mp3')
    time.sleep(3)
    dlSound('ふえふけどもおどらず', os.path.dirname(__file__) + os.sep + 'ふ.mp3')
    time.sleep(3)
    dlSound('ひとのうわさもしちじゅうごにち', os.path.dirname(__file__) + os.sep + 'ひ.mp3')
    time.sleep(3)
    dlSound('やまたかきがゆえにたっとからず', os.path.dirname(__file__) + os.sep + 'や.mp3')
