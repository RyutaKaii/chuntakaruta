import tkinter as tk
import os
import random
import time
from playsound import playsound
from PIL import ImageTk
import threading
import webbrowser
import sys

# ====================================================================================================
# かるたクラス
# ====================================================================================================
class Karuta:
    # ==================================================
    # コンストラクタ
    # ==================================================
    def __init__(self, page, headKana, kana, kanzi):
        # ページ
        self.page = page
        # 先頭かな
        self.headKana = headKana
        # かな
        self.kana = kana
        # 漢字
        self.kanzi = kanzi
        # 先頭かな音声ファイルパス
        self.headKanaAudioFPath = os.path.dirname(os.path.abspath(sys.argv[0])) + os.sep + 'audio' + os.sep + 'headKana' + os.sep + headKana + '.mp3'
        # かな音声ファイルパス
        self.kanaAudioFPath = os.path.dirname(os.path.abspath(sys.argv[0])) + os.sep + 'audio' + os.sep + 'kana' + os.sep + headKana + '.mp3'
    
    # ==================================================
    # 標準出力
    # ==================================================
    def debugprint(self):
        print('Karuta: ' + str(self.page) + ', ' + str(self.headKana) + ', ' + str(self.kana) + ', ' + str(self.kanzi) + ', ' + str(self.headKanaAudioFPath) + ', ' + str(self.kanaAudioFPath))

# ====================================================================================================
# page取得クラス
# ====================================================================================================
class GetPage:
    # ==================================================
    # コンストラクタ
    # ==================================================
    def __init__(self, start, end):
        # lengthの数字リスト
        self.pageList = []
        for i in range(start, end):
            self.pageList.append(i)
    
    # ==================================================
    # ページ取得
    # ==================================================
    def getPage(self):
        # ランダム取得
        page = random.choice(self.pageList)
        # 取得した要素を削除
        self.pageList.remove(page)

        return page

# ====================================================================================================
# 画面クラス
# ====================================================================================================
class Gui(tk.Frame):
    # ==================================================
    # コンストラクタ
    # ==================================================
    def __init__(self, master = None):
        super().__init__(master)

        # さいせいフラグ
        self.isPlay = False

        # タイトル
        self.master.title('Chuntaかるたよみあげ')
        # 画面サイズ
        self.master.geometry('900x400')
        # かるたリスト
        self.karutaList = crtKarutaList()
        # 現在ページ(page0は初期表示用)
        self.page = 0

        # 先頭かな
        self.headKanaLabel = tk.Label(self.master, text=self.karutaList[self.page].headKana, bg='pink', font=("MSゴシック", "30", "bold"))
        self.headKanaLabel.grid(row=0, column=0, padx=(10, 10), pady=(10, 10))
        # かな
        self.kanaLabel = tk.Label(self.master, text=self.karutaList[self.page].kana, bg='pink', font=("MSゴシック", "12", "bold"))
        self.kanaLabel.grid(row=0, column=1, padx=(10, 10), pady=(10, 10))
        # 漢字
        self.kanziLabel = tk.Label(self.master, text=self.karutaList[self.page].kanzi, bg='pink', font=("MSゴシック", "12", "bold"))
        self.kanziLabel.grid(row=0, column=2, padx=(10, 10), pady=(10, 10))
        # ページ
        self.pageLabel = tk.Label(self.master, text=str(self.page) + ' / 44', bg='pink')
        self.pageLabel.grid(row=1, column=0, columnspan=3, padx=(10, 10), pady=(10, 10))
        # チェックボックスの値
        self.chkValue = tk.BooleanVar(value = False)
        # チェックボックス
        self.chkboxIsSpeakHeadKana = tk.Checkbutton(self.master, text='さいしょのもじをよみあげる', variable = self.chkValue)
        self.chkboxIsSpeakHeadKana.grid(row=2, column=0, columnspan=3, padx=(10, 10), pady=(10, 10))
        # もどるボタン
        self.prevBtn = tk.Button(self.master, text='<< もどる', width=30, relief="raised", borderwidth = 3, command=self.prevBtnAction)
        self.prevBtn.grid(row=3, column=0, padx=(10, 10), pady=(10, 10))
        # くりかえしボタン
        self.speakAgainBtn = tk.Button(self.master, text='くりかえし', width=30, relief="raised", borderwidth = 3, command=self.speakAgainBtnAction)
        self.speakAgainBtn.grid(row=3, column=1, padx=(10, 10), pady=(10, 10))
        # よみあげボタン
        self.speakBtn = tk.Button(self.master, text='よみあげ >>', width=30, relief="raised", borderwidth = 3, bg='light cyan', command=self.speakBtnAction)
        self.speakBtn.grid(row=3, column=2, padx=(10, 10), pady=(10, 10))
        # ていしボタン
        self.stopBtn = tk.Button(self.master, text='■ ていし', width=50, relief="raised", borderwidth = 3, command=self.stopBtnAction)
        self.stopBtn.grid(row=4, column=0, columnspan=3, padx=(10, 10), pady=(10, 10))
        # さいせいボタン
        self.playBtn = tk.Button(self.master, text='▶ さいせい', width=50, relief="raised", borderwidth = 3, bg='light cyan', command=self.playBtnAction)
        self.playBtn.grid(row=5, column=0, columnspan=3, padx=(10, 10), pady=(10, 10))
        # ChuntaWeb画像
        self.titleCanvas = tk.Canvas(self.master, width=200, height=40, cursor='hand1')
        self.titleCanvas.grid(row=6, column=0,  columnspan=3)
        self.photoImage = ImageTk.PhotoImage(file = os.path.dirname(os.path.abspath(sys.argv[0])) + os.sep + 'img' + os.sep + 'title.png')
        self.titleCanvas.create_image(100, 20, image=self.photoImage)
        self.titleCanvas.bind("<Button-1>", lambda e:jumpLink('https://chuntaweb.sakura.ne.jp/'))
    
    # ==================================================
    # もどるボタン押下時の処理
    # ==================================================
    def prevBtnAction(self):
        print('>>>>>>>>>> prevBtnAction')

        if self.page == 0:
            return
        
        self.page -= 1

        self.disableAllBtn()
        speak(self.karutaList[self.page], self.chkValue.get())
        self.ableAllBtn()
        self.updateView()
    
    # ==================================================
    # くりかえしボタン押下時の処理
    # ==================================================
    def speakAgainBtnAction(self):
        print('>>>>>>>>>> speakAgainBtnAction')

        if self.page == -1:
            return
        
        self.disableAllBtn()
        speak(self.karutaList[self.page], self.chkValue.get())
        self.ableAllBtn()
    
    # ==================================================
    # よみあげボタン押下時の処理
    # ==================================================
    def speakBtnAction(self):
        print('>>>>>>>>>> speakBtnAction')

        if self.page == 44:
            return
        
        self.page += 1

        self.disableAllBtn()
        speak(self.karutaList[self.page], self.chkValue.get())
        self.ableAllBtn()
        self.updateView()
    
    # ==================================================
    # さいせいボタン押下時の処理
    # ==================================================
    def playBtnAction(self):
        print('>>>>>>>>>> playBtnAction')

        self.isPlay = True

        # スレッドキック
        thread = threading.Thread(target=self.playThread)
        thread.start()

        # 停止ボタン以外のボタンを非活性化
        self.prevBtn['state'] = tk.DISABLED
        self.speakAgainBtn['state'] = tk.DISABLED
        self.speakBtn['state'] = tk.DISABLED
        self.playBtn['state'] = tk.DISABLED
    
    # ==================================================
    # ていしボタン押下時の処理
    # ==================================================
    def stopBtnAction(self):
        print('>>>>>>>>>> stopBtnAction')

        self.ableAllBtn()

        self.isPlay = False
    
    # ==================================================
    # 表示更新
    # ==================================================
    def updateView(self):
        self.pageLabel.configure(text=str(self.page) + ' / 44')
        self.headKanaLabel.configure(text=self.karutaList[self.page].headKana)
        self.kanaLabel.configure(text=self.karutaList[self.page].kana)
        self.kanziLabel.configure(text=self.karutaList[self.page].kanzi)
    
    # ==================================================
    # 全てのボタンを非活性化
    # ==================================================
    def disableAllBtn(self):
        self.prevBtn['state'] = tk.DISABLED
        self.speakAgainBtn['state'] = tk.DISABLED
        self.speakBtn['state'] = tk.DISABLED
        self.stopBtn['state'] = tk.DISABLED
        self.playBtn['state'] = tk.DISABLED
        self.update()
    
    # ==================================================
    # 全てのボタンを活性化
    # ==================================================
    def ableAllBtn(self):
        self.prevBtn['state'] = tk.NORMAL
        self.speakAgainBtn['state'] = tk.NORMAL
        self.speakBtn['state'] = tk.NORMAL
        self.stopBtn['state'] = tk.NORMAL
        self.playBtn['state'] = tk.NORMAL
    
    # ==================================================
    # 再生スレッド
    # ==================================================
    def playThread(self):
        while True:
            if self.page == 44:
                break
        
            self.page += 1

            speak(self.karutaList[self.page], self.chkValue.get())
            self.updateView()

            time.sleep(10)

            if not self.isPlay:
                self.ableAllBtn()
                break

# ====================================================================================================
# かるたリストを生成
# ====================================================================================================
def crtKarutaList():
    getPage = GetPage(1, 45)

    karutaList = []

    # page0は初期表示用
    karutaList.append(Karuta(0, '', '', ''))

    karutaList.append(Karuta(getPage.getPage(), 'あ', 'あんずるがうむがやすし', '案ずるより産むが易し'))
    karutaList.append(Karuta(getPage.getPage(), 'い', 'いしのうえにもさんねん', '石の上にも三年'))
    karutaList.append(Karuta(getPage.getPage(), 'う', 'うごのたけのこ', '雨後のたけのこ'))
    karutaList.append(Karuta(getPage.getPage(), 'え', 'えびでたいをつる', '海老で鯛を釣る'))
    karutaList.append(Karuta(getPage.getPage(), 'お', 'おにのめにもなみだ', '鬼の目にも涙'))
    karutaList.append(Karuta(getPage.getPage(), 'か', 'かわいいこにはたびをさせよ', 'かわいい子には旅をさせよ'))
    karutaList.append(Karuta(getPage.getPage(), 'き', 'きをみてもりをみず', '木を見て森を見ず'))
    karutaList.append(Karuta(getPage.getPage(), 'く', 'くにやぶれてさんがあり', '国破れて山河在り'))
    karutaList.append(Karuta(getPage.getPage(), 'け', 'けいぞくはちからなり', '継続は力なり'))
    karutaList.append(Karuta(getPage.getPage(), 'こ', 'こうこうのしたいじぶんにおやはなし', '孝行のしたい時分に親はなし'))
    karutaList.append(Karuta(getPage.getPage(), 'さ', 'さるもきからおちる', '猿も木から落ちる'))
    karutaList.append(Karuta(getPage.getPage(), 'し', 'しゅんみんあかつきをおぼえず', '春眠暁を覚えず'))
    karutaList.append(Karuta(getPage.getPage(), 'す', 'すぎたるはなおおよばざるがごとし', '過ぎたるはなお及ばざるがごとし'))
    karutaList.append(Karuta(getPage.getPage(), 'せ', 'せいてはことをしそんじる', '急いては事を仕損じる'))
    karutaList.append(Karuta(getPage.getPage(), 'そ', 'そでふりあうもたしょうのえん', '袖振り合うも他生の縁'))
    karutaList.append(Karuta(getPage.getPage(), 'た', 'たなからぼたもち', '棚からぼたもち'))
    karutaList.append(Karuta(getPage.getPage(), 'ち', 'ちくばのとも', '竹馬の友'))
    karutaList.append(Karuta(getPage.getPage(), 'つ', 'つるはせんねんかめはまんねん', '鶴は千年亀は万年'))
    karutaList.append(Karuta(getPage.getPage(), 'て', 'てんたかくうまこゆるあき', '天高く馬肥ゆる秋'))
    karutaList.append(Karuta(getPage.getPage(), 'と', 'どんぐりのせいくらべ', 'どんぐりの背比べ'))
    karutaList.append(Karuta(getPage.getPage(), 'な', 'ならうよりなれよ', '習うより慣れよ'))
    karutaList.append(Karuta(getPage.getPage(), 'に', 'にかいからめぐすり', '二階から目薬'))
    karutaList.append(Karuta(getPage.getPage(), 'ぬ', 'ぬれぬさきのかさ', '濡れぬ先の傘'))
    karutaList.append(Karuta(getPage.getPage(), 'ね', 'ねこのてもかりたい', '猫の手も借りたい'))
    karutaList.append(Karuta(getPage.getPage(), 'の', 'のれんにうでおし', '暖簾に腕押し'))
    karutaList.append(Karuta(getPage.getPage(), 'は', 'はやおきはさんもんのとく', '早起きは三文の得'))
    karutaList.append(Karuta(getPage.getPage(), 'ひ', 'ひとのうわさもしちじゅうごにち', '人の噂も七十五日'))
    karutaList.append(Karuta(getPage.getPage(), 'ふ', 'ふえふけどもおどらず', '笛吹けども踊らず'))
    karutaList.append(Karuta(getPage.getPage(), 'へ', 'へたのかんがえやすむににたり', '下手の考え休むに似たり'))
    karutaList.append(Karuta(getPage.getPage(), 'ほ', 'ぼんとしょうがつがいっしょにきたよう', '盆と正月が一緒に来たよう'))
    karutaList.append(Karuta(getPage.getPage(), 'ま', 'まないたのこい', 'まな板の鯉'))
    karutaList.append(Karuta(getPage.getPage(), 'み', 'みずをえたさかなのよう', '水を得た魚のよう'))
    karutaList.append(Karuta(getPage.getPage(), 'む', 'むしのいどころがわるい', '虫の居所が悪い'))
    karutaList.append(Karuta(getPage.getPage(), 'め', 'めはくちほどにものをいう', '目は口ほどに物を言う'))
    karutaList.append(Karuta(getPage.getPage(), 'も', 'ももくりさんねんかきはちねん', '桃栗三年柿八年'))
    karutaList.append(Karuta(getPage.getPage(), 'や', 'やまたかきがゆえにたっとからず', '山高きがゆえに貴からず'))
    karutaList.append(Karuta(getPage.getPage(), 'ゆ', 'ゆをわかしてみずにする', '湯を沸かして水にする'))
    karutaList.append(Karuta(getPage.getPage(), 'よ', 'よくまなび、よくあそべ', 'よく学び、よく遊べ'))
    karutaList.append(Karuta(getPage.getPage(), 'ら', 'らいねんのことをいえばおにがわらう', '来年のことを言えば鬼が笑う'))
    karutaList.append(Karuta(getPage.getPage(), 'り', 'りょうてんびんにかける', '両てんびんにかける'))
    karutaList.append(Karuta(getPage.getPage(), 'る', 'るすみまいはまどおにせよ', '留守見舞いは間遠にせよ'))
    karutaList.append(Karuta(getPage.getPage(), 'れ', 'れきしはくりかえす', '歴史は繰り返す'))
    karutaList.append(Karuta(getPage.getPage(), 'ろ', 'ろくじゅうのてならい', '六十の手習い'))
    karutaList.append(Karuta(getPage.getPage(), 'わ', 'わをもってとうとしとなす', '和を以て貴しとなす'))

    # pageの昇順にソート
    karutaList = sorted(karutaList, key=lambda karuta: karuta.page)

    for karuta in karutaList:
        karuta.debugprint()

    return karutaList

# ====================================================================================================
# 読み上げ
# ====================================================================================================
def speak(karuta, isSpeakHeadKana):
    print('>>>>>>>>>> speak')
    karuta.debugprint()
    print('isSpeakHeadKana:' + str(isSpeakHeadKana))

    if isSpeakHeadKana:
        playsound(karuta.headKanaAudioFPath)
        time.sleep(1)
    
    playsound(karuta.kanaAudioFPath)

# ====================================================================================================
# ブラウザ起動してURLにジャンプ
# ====================================================================================================
def jumpLink(url):
    webbrowser.open_new(url)

# ====================================================================================================
# 直接実行時
# linux向け実行ファイル作成コマンド
# pyinstaller chuntaKaruta.py --onefile --noconsole --hidden-import='PIL._tkinter_finder'
# ====================================================================================================
if __name__ == "__main__":
    root = tk.Tk()
    app = Gui(master = root)
    app.mainloop()
